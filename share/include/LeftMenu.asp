   <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="/Main.asp">YOUNGLIM BATH</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
		
		<%
			sql = ""
			sql = sql & vbCrlf & "  select  a.code, a.title, a.icon "
			sql = sql & vbCrlf & "    from  sales_menu a with(nolock) "
			sql = sql & vbCrlf & "  where  a.status = 'Y' "
			sql = sql & vbCrlf & "     and  a.position = '1' "
			sql = sql & vbCrlf & "     and  charindex('" & Request.Cookies("msite") & "', a.site) > 0 "
			sql = sql & vbCrlf & "  order by  sort  "
			Set rs = db.execute(sql)
			Do Until rs.bof Or rs.eof
		%>
		
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="<%=rs("title")%>">
				  <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#<%=rs("code")%>" data-parent="#exampleAccordion">
					<i class="fa fa-fw <%=rs("icon")%>"></i>
					<span class="nav-link-text"><%=rs("title")%></span>
				  </a>
				  <ul class="sidenav-second-level collapse" id="<%=rs("code")%>">
					<%
						sql = ""
						sql = sql & vbCrlf & "  select  a.code, a.title, a.url, a.filename,   "
						sql = sql & vbCrlf & "  ( select  count(*)   "
                        sql = sql & vbCrlf & "      from  sales_menu z with(nolock)    "
                        sql = sql & vbCrlf & "    where  z.position = '3'   "
                        sql = sql & vbCrlf & "       and  z.top_p = a.code    "
                        sql = sql & vbCrlf & "       and  z.status = 'Y'    "
                        sql = sql & vbCrlf & "       and  charindex('" & Request.Cookies("msite") & "', a.site) > 0 ) as child_cnt "
						sql = sql & vbCrlf & "    from  sales_menu a with(nolock) "
						sql = sql & vbCrlf & "  where  a.status = 'Y' "
						sql = sql & vbCrlf & "     and  a.position = '2' "
						sql = sql & vbCrlf & "     and  a.top_p = '" & rs("code") & "' "
			            sql = sql & vbCrlf & "     and  charindex('" & Request.Cookies("msite") & "', a.site) > 0 "
						sql = sql & vbCrlf & "  order by  a.sort  "
						Set rs2 = db.execute(sql)
						Do Until rs2.bof Or rs2.eof
					%>
							<%
								If CInt(rs2("child_cnt")) > 0 then 
							%>
									<li>
									  <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#<%=rs2("code")%>"><%=rs2("title")%></a>
									  <ul class="sidenav-third-level collapse" id="<%=rs2("code")%>">
										<%
											sql = ""
											sql = sql & vbCrlf & "  select  a.code, a.title, a.url, a.filename "
											sql = sql & vbCrlf & "    from  sales_menu a with(nolock) "
											sql = sql & vbCrlf & "  where  a.status = 'Y' "
											sql = sql & vbCrlf & "     and  a.position = '3' "
											sql = sql & vbCrlf & "     and  a.top_p = '" & rs2("code") & "' "
                                			sql = sql & vbCrlf & "     and  charindex('" & Request.Cookies("msite") & "', a.site) > 0 "
											sql = sql & vbCrlf & "  order by  sort  "
											Set rs3 = db.execute(sql)
											Do Until rs3.bof Or rs3.eof
										%>
												<li>
												  <a href="/page/<%=rs3("url")%>/<%=rs3("filename")%>"><%=rs3("title")%></a>
												</li>
										<%
												rs3.movenext
											Loop 
											rs3.close
											Set rs3 = Nothing 
										%>
									  </ul>
									</li>
							<%
								Else 
							%>
									<li>
									  <a href="/page/<%=rs2("url")%>/<%=rs2("filename")%>"><%=rs2("title")%></a>
									</li>
							<%
								End If 
							%>
					<%
							rs2.movenext
						Loop 
						rs2.close
						Set rs2 = Nothing 
					%>
				  </ul>
				</li>
		<%
				rs.movenext
			Loop 
			rs.close
			Set rs = Nothing 
		%>

      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
	  
	  <!-- 모바일 화면시 활성화 시작 -->
      <ul class="navbar-nav ml-auto">

        <%
            If request.Cookies("id") = "admin" Then 
        %>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-fw fa-envelope"></i>
                    <span class="d-lg-none">Messages1212
                      <span class="badge badge-pill badge-primary">12 New</span>
                    </span>
                    <span class="indicator text-primary d-none d-lg-block">
                      <i class="fa fa-fw fa-circle"></i>
                    </span>
                  </a>
                  <div class="dropdown-menu" aria-labelledby="messagesDropdown">
                    <h6 class="dropdown-header">New Messages:</h6>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                      <strong>David Miller</strong>
                      <span class="small float-right text-muted">11:21 AM</span>
                      <div class="dropdown-message small">Hey there! This new version of SB Admin is pretty awesome! These messages clip off when they reach the end of the box so they don't overflow over to the sides!</div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                      <strong>Jane Smith</strong>
                      <span class="small float-right text-muted">11:21 AM</span>
                      <div class="dropdown-message small">I was wondering if you could meet for an appointment at 3:00 instead of 4:00. Thanks!</div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                      <strong>John Doe</strong>
                      <span class="small float-right text-muted">11:21 AM</span>
                      <div class="dropdown-message small">I've sent the final files over to you for review. When you're able to sign off of them let me know and we can discuss distribution.</div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item small" href="#">View all messages</a>
                  </div>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-fw fa-bell"></i>
                    <span class="d-lg-none">Alerts
                      <span class="badge badge-pill badge-warning">6 New</span>
                    </span>
                    <span class="indicator text-warning d-none d-lg-block">
                      <i class="fa fa-fw fa-circle"></i>
                    </span>
                  </a>
                  <div class="dropdown-menu" aria-labelledby="alertsDropdown">
                    <h6 class="dropdown-header">New Alerts:</h6>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                      <span class="text-success">
                        <strong>
                          <i class="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>
                      </span>
                      <span class="small float-right text-muted">11:21 AM</span>
                      <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                      <span class="text-danger">
                        <strong>
                          <i class="fa fa-long-arrow-down fa-fw"></i>Status Update</strong>
                      </span>
                      <span class="small float-right text-muted">11:21 AM</span>
                      <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                      <span class="text-success">
                        <strong>
                          <i class="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>
                      </span>
                      <span class="small float-right text-muted">11:21 AM</span>
                      <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item small" href="#">View all alerts</a>
                  </div>
                </li>
                <li class="nav-item">
                  <form class="form-inline my-2 my-lg-0 mr-lg-2">
                    <div class="input-group">
                      <input class="form-control" type="text" placeholder="Search for...">
                      <span class="input-group-append">
                        <button class="btn btn-primary" type="button">
                          <i class="fa fa-search"></i>
                        </button>
                      </span>
                    </div>
                  </form>
                </li>
        <%
            End If
        %>

        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#LogoutModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
	  <!-- 모바일 화면시 활성화 끝 -->

    </div>
  </nav>
