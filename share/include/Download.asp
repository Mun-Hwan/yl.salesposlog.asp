<%
	response.expires = -1
	response.AddHeader "Pragma", "no-cache"
	response.AddHeader "cache-control", "no-store"
	response.buffer = true
	response.expiresabsolute = #1999/10/12# ' 캐시를 죽여주어 새페이지가 로드되게 해준다.

	path = Request("p")
	filename = Request("f")

	DirectoryPath = Server.MapPath(path)
	realPath = DirectoryPath & "\" & filename

    Response.ContentType = "application/octect-stream"
    Response.AddHeader "Content-Disposition","attachment; filename=" & filename

    Set objStream = Server.CreateObject("ADODB.Stream")
    objStream.Open
    objStream.Type = 1
    objStream.LoadFromFile realPath
    download = objStream.Read
    Response.BinaryWrite download
    Set objstream = nothing 
%> 
