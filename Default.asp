<%@Language="VBScript" CODEPAGE="65001"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><%=SITE_TITLE%></title>
  <!-- Bootstrap core CSS-->
  <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="/assets/css/sb-admin.css" rel="stylesheet">

  <style type="text/css">
		.logo {
			width: 200px;
		}

		.form-check-label {
			font-size: 9pt;
		}
  </style>
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header text-center">
		<img class="logo" src="share/images/logo_ylbath.png"/>
	  </div>
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputEmail1">아이디</label>
          <input class="form-control" id="exampleInputEmail1" type="text" aria-describedby="emailHelp" placeholder="ID" value="admin">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">비밀번호</label>
          <input class="form-control" id="exampleInputPassword1" type="password" placeholder="Password"  value="admin">
        </div>
		<button type="button" class="btn btn-primary btn-block btn-lg" onclick="loginAjax();">Login</button>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="assets/js/jquery.backstretch.min.js"></script>
  <!-- Younglim plugin JavaScript -->
  <script src="share/js/jquery.ylbath.script.js"></script>
  <script src="share/js/jquery.login.script.js"></script>

  <script type="text/javascript">
  <!--
		$(document).ready(function(){
			$("#exampleInputEmail1").focus();
		});

		$("input").keypress( function(e) {
			if (e.keyCode == 13) {
				loginAjax();
				e.preventDefault();
			}
		});

		function loginAjax() {
			$.ajax({
				type : "post",
				data: {
						id: $("#exampleInputEmail1").val(),
						pwd: $("#exampleInputPassword1").val()
				},
				url: "/data/Login/Login.Proc.asp",
				async: false,
				dataType: "text",
                success: function (data) {
					if (data == "OK") {
						location.href = "/page/LOG/Sales.List.asp";
					} else {
						alert("로그인 정보가 틀렸습니다.");
					}
				},
				error: function(request, status, error) {
					alert("code : " + request.status + "\n" + "message : " + request.responseText + "\n" + "error : " + error);
				}
			});
		}
  //-->
  </script>
</body>

</html>
