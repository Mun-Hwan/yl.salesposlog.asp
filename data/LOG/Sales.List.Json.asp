<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_2.0.4.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_UTIL_0.1.1.asp" -->
<%
	startDate = str_chk(Request("startDate"))
	endDate = str_chk(Request("endDate"))
	BusiGubun = str_chk(Request("BusiGubun"))
	Page = str_chk(Request("Page"))
	PageSize = str_chk(Request("PageSize"))
	
	
	strWhere = ""
	strWhere = strWhere & vbCrlf & "                   and  a.bb_visit_date >= '" & startDate & "'  "
	strWhere = strWhere & vbCrlf & "                   and  a.bb_visit_date < '" & DateAdd("d", 1, endDate) & "'  "
	'strWhere = strWhere & vbCrlf & "                   and  b.mb_level in ('7', '67')  "
	Select Case BusiGubun
		Case "YLF"
			strWhere = strWhere & vbCrlf & "				  and  (select z.cd_name from tb_code z where z.cd_seq = a.s_seq and z.cd_type = 'SALES') like '임업_%'		"

		Case "YLC"
			strWhere = strWhere & vbCrlf & "				  and  (select z.cd_name from tb_code z where z.cd_seq = a.s_seq and z.cd_type = 'SALES') like '화학_%'		"

		Case "KB"
			strWhere = strWhere & vbCrlf & "				  and  (select z.cd_name from tb_code z where z.cd_seq = a.s_seq and z.cd_type = 'SALES') like '키친바스'		"

		Case Else
			strWhere = strWhere & vbCrlf & "				  and  (     "
			strWhere = strWhere & vbCrlf & "				                     (select z.cd_name from tb_code z where z.cd_seq = a.s_seq and z.cd_type = 'SALES') like '임업_%'		"
			strWhere = strWhere & vbCrlf & "				               or   (select z.cd_name from tb_code z where z.cd_seq = a.s_seq and z.cd_type = 'SALES') like '화학_%'		"
			strWhere = strWhere & vbCrlf & "				               or   (select z.cd_name from tb_code z where z.cd_seq = a.s_seq and z.cd_type = 'SALES') like '키친바스'		"
			strWhere = strWhere & vbCrlf & "				          )     "
		
	End Select 

	RecordCount = 0
	sql = ""
	sql = sql & vbCrlf & "          select    count(*) as Cnt		"
	sql = sql & vbCrlf & "             from  tb_board_basic a,		"
	sql = sql & vbCrlf & "                      tb_member b		"
	sql = sql & vbCrlf & "           where  a.mb_seq = b.mb_seq		"
	sql = sql & vbCrlf & strWhere
	'Response.Write sql
    Set rs = db.Execute(sql)
	If Not rs.bof And Not rs.eof Then  RecordCount = CInt(rs("Cnt"))



	nPageSize = CInt("0" & PageSize)
	If page < "1" Then
		page = 1
	End If

	'startNum = ((page - 1) * nPageSize) + 1
	'endNum = ((page - 1) * nPageSize) + nPageSize 
	startNum = ((page - 1) * nPageSize)

	sql = ""
	sql = sql & vbCrlf & "          select    " & RecordCount & " as max_cnt ,		"
	sql = sql & vbCrlf & "                      (select z.cd_name from tb_code z where z.cd_seq = a.s_seq and z.cd_type = 'SALES') as s_seq_nm ,		"
	sql = sql & vbCrlf & "                      (select z.cd_name from tb_code z where z.cd_seq = a.bb_code and z.cd_type = 'TYPE') as bb_code_nm ,		"
	sql = sql & vbCrlf & "                      a.bb_name,		"
	sql = sql & vbCrlf & "                      b.mb_position,		"
	sql = sql & vbCrlf & "                      a.bb_agency_name,		"
	sql = sql & vbCrlf & "                      cast(a.bb_visit_date as date) as bb_visit_date,		"
	sql = sql & vbCrlf & "                      cast(a.bb_visit_date as datetime) as bb_visit_time,		"
	sql = sql & vbCrlf & "                      a.bb_txt		"
	sql = sql & vbCrlf & "             from  tb_board_basic a,		"
	sql = sql & vbCrlf & "                      tb_member b		"
	sql = sql & vbCrlf & "           where  a.mb_seq = b.mb_seq		"
	sql = sql & vbCrlf & strWhere
	sql = sql & vbCrlf & "        order by  bb_visit_date,		"
	sql = sql & vbCrlf & "                       a.bb_name,		"
	sql = sql & vbCrlf & "                       bb_visit_time,		"
	sql = sql & vbCrlf & "                       bb_agency_name		"
	sql = sql & vbCrlf & "               limit  " & startNum & ", " & PageSize & "			"
	'Response.Write sql

	QueryToJSON(db, sql).Flush

%>
<!-- #include virtual = "/share/include/DbClose.asp" -->
