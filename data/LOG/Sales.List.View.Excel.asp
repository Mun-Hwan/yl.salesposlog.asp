<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<%

	startDate = str_chk(Request("startDate"))
	endDate = str_chk(Request("endDate"))
	BusiGubun = str_chk(Request("BusiGubun"))

	xml_str = ""
	xml_str = xml_str & vbCrlf & "  <?xml version=""1.0""?>"
	xml_str = xml_str & vbCrlf & "  <?mso-application progid=""Excel.Sheet""?>"
	xml_str = xml_str & vbCrlf & "  <Workbook xmlns=""urn:schemas-microsoft-com:office:spreadsheet"""
	xml_str = xml_str & vbCrlf & "   xmlns:o=""urn:schemas-microsoft-com:office:office"""
	xml_str = xml_str & vbCrlf & "   xmlns:x=""urn:schemas-microsoft-com:office:excel"""
	xml_str = xml_str & vbCrlf & "   xmlns:ss=""urn:schemas-microsoft-com:office:spreadsheet"""
	xml_str = xml_str & vbCrlf & "   xmlns:html=""http://www.w3.org/TR/REC-html40"">"
	xml_str = xml_str & vbCrlf & "   <DocumentProperties xmlns=""urn:schemas-microsoft-com:office:office"">"
	xml_str = xml_str & vbCrlf & "    <Author></Author>"
	xml_str = xml_str & vbCrlf & "    <LastAuthor></LastAuthor>"
	xml_str = xml_str & vbCrlf & "    <LastPrinted></LastPrinted>"
	xml_str = xml_str & vbCrlf & "    <Created></Created>"
	xml_str = xml_str & vbCrlf & "    <LastSaved></LastSaved>"
	xml_str = xml_str & vbCrlf & "    <Version>16.00</Version>"
	xml_str = xml_str & vbCrlf & "   </DocumentProperties>"
	xml_str = xml_str & vbCrlf & "   <OfficeDocumentSettings xmlns=""urn:schemas-microsoft-com:office:office"">"
	xml_str = xml_str & vbCrlf & "    <AllowPNG/>"
	xml_str = xml_str & vbCrlf & "   </OfficeDocumentSettings>"
	xml_str = xml_str & vbCrlf & "   <ExcelWorkbook xmlns=""urn:schemas-microsoft-com:office:excel"">"
	xml_str = xml_str & vbCrlf & "    <WindowHeight>12285</WindowHeight>"
	xml_str = xml_str & vbCrlf & "    <WindowWidth>28800</WindowWidth>"
	xml_str = xml_str & vbCrlf & "    <WindowTopX>0</WindowTopX>"
	xml_str = xml_str & vbCrlf & "    <WindowTopY>0</WindowTopY>"
	xml_str = xml_str & vbCrlf & "    <Calculation>ManualCalculation</Calculation>"
	xml_str = xml_str & vbCrlf & "    <ProtectStructure>False</ProtectStructure>"
	xml_str = xml_str & vbCrlf & "    <ProtectWindows>False</ProtectWindows>"
	xml_str = xml_str & vbCrlf & "   </ExcelWorkbook>"
	xml_str = xml_str & vbCrlf & "   <Styles>"
	xml_str = xml_str & vbCrlf & "    <Style ss:ID=""Default"" ss:Name=""Normal"">"
	xml_str = xml_str & vbCrlf & "     <Alignment ss:Vertical=""Center""/>"
	xml_str = xml_str & vbCrlf & "     <Borders/>"
	xml_str = xml_str & vbCrlf & "     <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Size=""11"""
	xml_str = xml_str & vbCrlf & "      ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "     <Interior/>"
	xml_str = xml_str & vbCrlf & "     <NumberFormat/>"
	xml_str = xml_str & vbCrlf & "     <Protection/>"
	xml_str = xml_str & vbCrlf & "    </Style>"
	xml_str = xml_str & vbCrlf & "    <Style ss:ID=""s64"">"
	xml_str = xml_str & vbCrlf & "     <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Size=""24"""
	xml_str = xml_str & vbCrlf & "      ss:Color=""#000000"" ss:Bold=""1""/>"
	xml_str = xml_str & vbCrlf & "    </Style>"
	xml_str = xml_str & vbCrlf & "    <Style ss:ID=""s88"">"
	xml_str = xml_str & vbCrlf & "     <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    </Style>"
	xml_str = xml_str & vbCrlf & "    <Style ss:ID=""s102"">"
	xml_str = xml_str & vbCrlf & "     <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center""/>"
	xml_str = xml_str & vbCrlf & "     <Borders>"
	xml_str = xml_str & vbCrlf & "      <Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>"
	xml_str = xml_str & vbCrlf & "      <Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>"
	xml_str = xml_str & vbCrlf & "      <Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>"
	xml_str = xml_str & vbCrlf & "      <Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>"
	xml_str = xml_str & vbCrlf & "     </Borders>"
	xml_str = xml_str & vbCrlf & "     <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Color=""#000000"""
	xml_str = xml_str & vbCrlf & "      ss:Bold=""1""/>"
	xml_str = xml_str & vbCrlf & "     <Interior ss:Color=""#BFBFBF"" ss:Pattern=""Solid""/>"
	xml_str = xml_str & vbCrlf & "    </Style>"
	xml_str = xml_str & vbCrlf & "    <Style ss:ID=""s105"">"
	xml_str = xml_str & vbCrlf & "     <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center""/>"
	xml_str = xml_str & vbCrlf & "     <Borders>"
	xml_str = xml_str & vbCrlf & "      <Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>"
	xml_str = xml_str & vbCrlf & "      <Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>"
	xml_str = xml_str & vbCrlf & "      <Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>"
	xml_str = xml_str & vbCrlf & "      <Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>"
	xml_str = xml_str & vbCrlf & "     </Borders>"
	xml_str = xml_str & vbCrlf & "     <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    </Style>"
	xml_str = xml_str & vbCrlf & "    <Style ss:ID=""s110"">"
	xml_str = xml_str & vbCrlf & "     <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center""/>"
	xml_str = xml_str & vbCrlf & "     <Borders/>"
	xml_str = xml_str & vbCrlf & "     <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Size=""20"""
	xml_str = xml_str & vbCrlf & "      ss:Color=""#000000"" ss:Bold=""1""/>"
	xml_str = xml_str & vbCrlf & "    </Style>"
	xml_str = xml_str & vbCrlf & "    <Style ss:ID=""s114"">"
	xml_str = xml_str & vbCrlf & "     <Alignment ss:Vertical=""Center"" ss:WrapText=""1""/>"
	xml_str = xml_str & vbCrlf & "     <Borders>"
	xml_str = xml_str & vbCrlf & "      <Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>"
	xml_str = xml_str & vbCrlf & "      <Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>"
	xml_str = xml_str & vbCrlf & "      <Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>"
	xml_str = xml_str & vbCrlf & "      <Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1""/>"
	xml_str = xml_str & vbCrlf & "     </Borders>"
	xml_str = xml_str & vbCrlf & "     <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    </Style>"
	xml_str = xml_str & vbCrlf & "   </Styles>"
	xml_str = xml_str & vbCrlf & "   <Worksheet ss:Name=""Sheet1"">"
	xml_str = xml_str & vbCrlf & "    <Names>"
	xml_str = xml_str & vbCrlf & "     <NamedRange ss:Name=""Print_Titles"" ss:RefersTo=""=Sheet1!R1:R2""/>"
	xml_str = xml_str & vbCrlf & "    </Names>"
	xml_str = xml_str & vbCrlf & "    <Table ss:ExpandedColumnCount=""7"" ss:ExpandedRowCount=""99999"" x:FullColumns=""1"""
	xml_str = xml_str & vbCrlf & "     x:FullRows=""1"" ss:DefaultColumnWidth=""54"" ss:DefaultRowHeight=""16.5"">"
	xml_str = xml_str & vbCrlf & "     <Column ss:AutoFitWidth=""0"" ss:Width=""105.75"" ss:Span=""2""/>"
	xml_str = xml_str & vbCrlf & "     <Column ss:Index=""4"" ss:Width=""144.75""/>"
	xml_str = xml_str & vbCrlf & "     <Column ss:Width=""138""/>"
	xml_str = xml_str & vbCrlf & "     <Column ss:Width=""144.75""/>"
	xml_str = xml_str & vbCrlf & "     <Column ss:AutoFitWidth=""0"" ss:Width=""379.5""/>"
	xml_str = xml_str & vbCrlf & "     <Row ss:AutoFitHeight=""0"" ss:Height=""51"" ss:StyleID=""s64"">"

	Title = ""
	Select Case BusiGubun
		Case "YLF"
			Title = "임업"
		Case "YLC"
			Title = "화학"
		Case "KB"
			Title = "키친바스"
		Case Else 
			Title = "임업, 화학, 키친바스"
	End Select 

	xml_str = xml_str & vbCrlf & "      <Cell ss:MergeAcross=""6"" ss:StyleID=""s110""><Data ss:Type=""String"">영림" & Title & " 수도권 영업부 대리점 방문 일일보고</Data></Cell>"
	xml_str = xml_str & vbCrlf & "     </Row>"
	xml_str = xml_str & vbCrlf & "     <Row ss:AutoFitHeight=""0"" ss:Height=""23.25"" ss:StyleID=""s88"">"
	xml_str = xml_str & vbCrlf & "      <Cell ss:StyleID=""s102""><Data ss:Type=""String"">권역</Data></Cell>"
	xml_str = xml_str & vbCrlf & "      <Cell ss:StyleID=""s102""><Data ss:Type=""String"">구분</Data></Cell>"
	xml_str = xml_str & vbCrlf & "      <Cell ss:StyleID=""s102""><Data ss:Type=""String"">이름</Data></Cell>"
	xml_str = xml_str & vbCrlf & "      <Cell ss:StyleID=""s102""><Data ss:Type=""String"">대리점</Data></Cell>"
	xml_str = xml_str & vbCrlf & "      <Cell ss:StyleID=""s102""><Data ss:Type=""String"">출발시간</Data></Cell>"
	xml_str = xml_str & vbCrlf & "      <Cell ss:StyleID=""s102""><Data ss:Type=""String"">완료시간</Data></Cell>"
	xml_str = xml_str & vbCrlf & "      <Cell ss:StyleID=""s102""><Data ss:Type=""String"">방문내용</Data></Cell>"
	xml_str = xml_str & vbCrlf & "     </Row>"

	strWhere = ""
	strWhere = strWhere & vbCrlf & "                   and  a.bb_visit_date >= '" & startDate & "'  "
	strWhere = strWhere & vbCrlf & "                   and  a.bb_visit_date < '" & DateAdd("d", 1, endDate) & "'  "
	'strWhere = strWhere & vbCrlf & "                   and  b.mb_level in ('7', '67')  "
	Select Case BusiGubun
		Case "YLF"
			strWhere = strWhere & vbCrlf & "				  and  (select z.cd_name from tb_code z where z.cd_seq = a.s_seq and z.cd_type = 'SALES') like '임업_%'		"

		Case "YLC"
			strWhere = strWhere & vbCrlf & "				  and  (select z.cd_name from tb_code z where z.cd_seq = a.s_seq and z.cd_type = 'SALES') like '화학_%'		"

		Case "KB"
			strWhere = strWhere & vbCrlf & "				  and  (select z.cd_name from tb_code z where z.cd_seq = a.s_seq and z.cd_type = 'SALES') like '키친바스'		"

		Case Else
			strWhere = strWhere & vbCrlf & "				  and  (     "
			strWhere = strWhere & vbCrlf & "				                     (select z.cd_name from tb_code z where z.cd_seq = a.s_seq and z.cd_type = 'SALES') like '임업_%'		"
			strWhere = strWhere & vbCrlf & "				               or   (select z.cd_name from tb_code z where z.cd_seq = a.s_seq and z.cd_type = 'SALES') like '화학_%'		"
			strWhere = strWhere & vbCrlf & "				               or   (select z.cd_name from tb_code z where z.cd_seq = a.s_seq and z.cd_type = 'SALES') like '키친바스'		"
			strWhere = strWhere & vbCrlf & "				          )     "
		
	End Select 

	sql = ""
	sql = sql & vbCrlf & "          select    (select REPLACE(z.cd_name, '임업_', '') from tb_code z where z.cd_seq = a.s_seq and z.cd_type = 'SALES') as s_seq_nm ,		"
	sql = sql & vbCrlf & "                      (select z.cd_name from tb_code z where z.cd_seq = a.bb_code and z.cd_type = 'TYPE') as bb_code_nm ,		"
	sql = sql & vbCrlf & "                      a.bb_name,		"
	sql = sql & vbCrlf & "                      b.mb_position,		"
	sql = sql & vbCrlf & "                      a.bb_agency_name,		"
	sql = sql & vbCrlf & "                      cast(a.bb_visit_date as date) as bb_visit_date,		"
	sql = sql & vbCrlf & "                      cast(a.bb_visit_date as datetime) as bb_visit_time,		"
	sql = sql & vbCrlf & "                      a.bb_txt		"
	sql = sql & vbCrlf & "             from  tb_board_basic a,		"
	sql = sql & vbCrlf & "                      tb_member b		"
	sql = sql & vbCrlf & "           where  a.mb_seq = b.mb_seq		"
	'sql = sql & vbCrlf & "              and  a.bb_name in ('김병규', '김진')		"
	sql = sql & vbCrlf & strWhere
	sql = sql & vbCrlf & "        order by  bb_visit_date,		"
	sql = sql & vbCrlf & "                       a.bb_name,		"
	sql = sql & vbCrlf & "                       bb_visit_time,		"
	sql = sql & vbCrlf & "                       bb_agency_name		"
	'Response.Write sql
	Set rs = db.execute (sql)
	agency = ""
	Do Until rs.bof Or rs.eof

		sSeqNm = ("" & rs("s_seq_nm"))
		sSeqNm = Replace(sSeqNm, "임업_", "")
		sSeqNm = Replace(sSeqNm, "화학_", "")

		If agency <> rs("bb_agency_name") Then 
			agency = rs("bb_agency_name")
			
			xml_str = Replace(xml_str, "#COMPLETE_TIME#", "")
			xml_str = Replace(xml_str, "&#10;#COMPLETE_TXT#", "")

			xml_str = xml_str & vbCrlf & "     <Row ss:Height=""40.5"" ss:StyleID=""s88"">"
			xml_str = xml_str & vbCrlf & "      <Cell ss:StyleID=""s105""><Data ss:Type=""String"">" & sSeqNm & "</Data></Cell>"
			xml_str = xml_str & vbCrlf & "      <Cell ss:StyleID=""s105""><Data ss:Type=""String"">" & rs("bb_code_nm") & "</Data></Cell>"
			xml_str = xml_str & vbCrlf & "      <Cell ss:StyleID=""s105""><Data ss:Type=""String"">" & rs("bb_name") & "</Data></Cell>"
			xml_str = xml_str & vbCrlf & "      <Cell ss:StyleID=""s105""><Data ss:Type=""String"">" & rs("bb_agency_name") & "</Data></Cell>"
			xml_str = xml_str & vbCrlf & "      <Cell ss:StyleID=""s105""><Data ss:Type=""String"">" & Mid(rs("bb_visit_time"), 1, Len(rs("bb_visit_time")) - 3) & "</Data></Cell>"
			xml_str = xml_str & vbCrlf & "      <Cell ss:StyleID=""s105""><Data ss:Type=""String"">#COMPLETE_TIME#</Data></Cell>"
			xml_str = xml_str & vbCrlf & "      <Cell ss:StyleID=""s114""><Data ss:Type=""String"">" & rs("bb_txt") & "&#10;#COMPLETE_TXT#</Data></Cell>"
			xml_str = xml_str & vbCrlf & "     </Row>"
		Else
			xml_str = Replace(xml_str, "#COMPLETE_TIME#", Mid(rs("bb_visit_time"), 1, Len(rs("bb_visit_time")) - 3))
			xml_str = Replace(xml_str, "#COMPLETE_TXT#", rs("bb_txt"))
		End If

		rs.movenext
	Loop 

	xml_str = Replace(xml_str, "#COMPLETE_TIME#", "")
	xml_str = Replace(xml_str, "&#10;#COMPLETE_TXT#", "")

	rs.close
	Set rs = Nothing 

	xml_str = xml_str & vbCrlf & "    </Table>"
	xml_str = xml_str & vbCrlf & "    <WorksheetOptions xmlns=""urn:schemas-microsoft-com:office:excel"">"
	xml_str = xml_str & vbCrlf & "     <PageSetup>"
	xml_str = xml_str & vbCrlf & "      <Layout x:Orientation=""Landscape""/>"
	xml_str = xml_str & vbCrlf & "      <Header x:Margin=""0""/>"
	xml_str = xml_str & vbCrlf & "      <Footer x:Margin=""0"" x:Data=""&amp;C&amp;P / &amp;N""/>"
	xml_str = xml_str & vbCrlf & "      <PageMargins x:Bottom=""0.39370078740157483"" x:Left=""0.39370078740157483"""
	xml_str = xml_str & vbCrlf & "       x:Right=""0.39370078740157483"" x:Top=""0.39370078740157483""/>"
	xml_str = xml_str & vbCrlf & "     </PageSetup>"
	xml_str = xml_str & vbCrlf & "     <FitToPage/>"
	xml_str = xml_str & vbCrlf & "     <Print>"
	xml_str = xml_str & vbCrlf & "      <FitHeight>0</FitHeight>"
	xml_str = xml_str & vbCrlf & "      <ValidPrinterInfo/>"
	xml_str = xml_str & vbCrlf & "      <PaperSizeIndex>9</PaperSizeIndex>"
	xml_str = xml_str & vbCrlf & "      <Scale>68</Scale>"
	xml_str = xml_str & vbCrlf & "      <HorizontalResolution>600</HorizontalResolution>"
	xml_str = xml_str & vbCrlf & "      <VerticalResolution>600</VerticalResolution>"
	xml_str = xml_str & vbCrlf & "     </Print>"
	xml_str = xml_str & vbCrlf & "     <Selected/>"
	xml_str = xml_str & vbCrlf & "     <FreezePanes/>"
	xml_str = xml_str & vbCrlf & "     <FrozenNoSplit/>"
	xml_str = xml_str & vbCrlf & "     <SplitHorizontal>2</SplitHorizontal>"
	xml_str = xml_str & vbCrlf & "     <TopRowBottomPane>2</TopRowBottomPane>"
	xml_str = xml_str & vbCrlf & "     <ActivePane>2</ActivePane>"
	xml_str = xml_str & vbCrlf & "     <Panes>"
	xml_str = xml_str & vbCrlf & "      <Pane>"
	xml_str = xml_str & vbCrlf & "       <Number>3</Number>"
	xml_str = xml_str & vbCrlf & "      </Pane>"
	xml_str = xml_str & vbCrlf & "      <Pane>"
	xml_str = xml_str & vbCrlf & "       <Number>2</Number>"
	xml_str = xml_str & vbCrlf & "       <ActiveRow>0</ActiveRow>"
	xml_str = xml_str & vbCrlf & "       <RangeSelection>R1C1:R1C7</RangeSelection>"
	xml_str = xml_str & vbCrlf & "      </Pane>"
	xml_str = xml_str & vbCrlf & "     </Panes>"
	xml_str = xml_str & vbCrlf & "     <ProtectObjects>False</ProtectObjects>"
	xml_str = xml_str & vbCrlf & "     <ProtectScenarios>False</ProtectScenarios>"
	xml_str = xml_str & vbCrlf & "    </WorksheetOptions>"
	xml_str = xml_str & vbCrlf & "   </Worksheet>"
	xml_str = xml_str & vbCrlf & "  </Workbook>"

	response.write xml_str

	filename = "대리점방문내역 [" & Replace(startDate, "-" , "") & "-" & Replace(endDate, "-" , "") & "]"

	response.expires = 0
	response.buffer = true
	'Response.ContentType  = "application/x-msexcel" ' xls
	'Response.ContentType  = "application/vnd.ms-excel" ' xlsx				'// 이거 사용시 파일을 읽을 수 없습니다. 오류가 종종 발생함.
	Response.ContentType  = "application/vnd.xls" ' xlsx
	Response.CacheControl = "public"
	Response.AddHeader  "Content-Disposition" , "attachment; filename=" & filename & ".xls"
%>

<!-- #include virtual = "/share/include/DbClose.asp" -->
