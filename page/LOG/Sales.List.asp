<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/PageHeader.asp" -->
<!-- #include virtual = "/share/include/ContentsHeader.asp" -->

<%
        '// 기본날짜 셋팅
        stDt = Date()
        edDt = Date()

        '// Paging 기준값
        PageSize = 50           '// 페이지당 보여줄 데이타수
        BlockSize = 5             '// 페이지 그룹 범위       1 2 3 5 6 7 8 9 10
%>

    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">영림임업</a>
        </li>
        <li class="breadcrumb-item active">대리점 방문 일일보고</li>
      </ol>

	  <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
			<form id="Search_Form">
                <input type="hidden" class="form-control" name="Page" value="1">
                <input type="hidden" class="form-control" name="PageSize" value="<%=PageSize%>">
				<div class="row justify-content-between">
                    <div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">방문일</span>
							</div>
							<input type="text" class="form-control text-center" id="startDate" value="<%=stDt%>" required>
							<input type="text" class="form-control text-center" id="endDate" value="<%=edDt%>" required>
						</div>
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">구분</span>
							</div>
							<select class="form-control" name="BusiGubun" id="BusiGubun">
								<option value=""></option>
								<option value="YLF">임업</option>
								<option value="YLC">화학</option>
								<option value="KB">키친바스</option>
							</select>
						</div>
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
						<button type="submit" class="btn btn-secondary mary btn-block" onclick="$('#Search_Form').find('input[name=Page]').val(1);"><i class="fa fa-search" aria-hidden="true"></i> <span>조회</span></button>
                     </div>
                </div>
			</form>

		</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" style="font-size: 8pt;" id="TransTable">
                    <thead>
                        <tr>
						    <th scope="col" width="10%">권역</th>
						    <th scope="col" width="10%">구분</th>
						    <th scope="col" width="10%">이름</th>
						    <th scope="col" width="10%">대리점</th>
						    <th scope="col" width="10%">출발시간</th>
						    <th scope="col" width="10%">완료시간</th>
						    <th scope="col" width="40%">방문내용</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                 </table>
            </div>

        </div>
        <div class="card-footer" id="Paging_MainTransForm">
		</div>
        <div class="card-footer">
            <button type="button" id="btnExcel" class='btn btn-info'><i class='fa fa-file-excel-o' aria-hidden='true'></i> <span>대리점 방문 일일보고</span></button>
            <button type="button" id="btnViewExcel" class='btn btn-warning'><i class='fa fa-file-excel-o' aria-hidden='true'></i> <span>화면 엑셀받기</span></button>
		</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->


<!-- #include virtual = "/share/include/ContentsAddon.asp" -->
<!-- #include virtual = "/share/include/ContentsFooter.asp" -->
<!-- #include virtual = "/share/include/JavaScript.asp" -->

<script type="text/javascript">
<!--
	$(document).ready(function() {

        $('#startDate').datepicker({
			format: "yyyy-mm-dd",
			todayBtn: "linked",
			language: "kr",
			multidate: false,
			//daysOfWeekDisabled: "0",
			daysOfWeekHighlighted: "0",
			autoclose: true,
			todayHighlight: true,
			//datesDisabled: ['07/06/2018', '07/21/2018'],
			toggleActive: true
        });

        $('#endDate').datepicker({
			format: "yyyy-mm-dd",
			todayBtn: "linked",
			language: "kr",
			multidate: false,
			//daysOfWeekDisabled: "0",
			daysOfWeekHighlighted: "0",
			autoclose: true,
			todayHighlight: true,
			//datesDisabled: ['07/06/2018', '07/21/2018'],
			toggleActive: true
        });

		$("#Search_Form").ajaxForm({
			url: "/data/LOG/Sales.List.Json.asp",
            data: {
                startDate : function() { return $("#startDate").val() }, 
                endDate: function () { return $("#endDate").val() }
            },
            type: "POST",
			dataType: "json",
			beforeSend:function(){
                $(".fa").addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".fa").removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(data, status){
                var info = "";
                var maxCnt = 0;
				var agency = "";
				var temp = "";
                $.each(data, function () {
                    maxCnt = this.max_cnt;

					if (agency != this.bb_agency_name) {
	                    //info += temp;
						agency = this.bb_agency_name;

						info = info.replace("#COMPLETE_TIME#", "");
						info = info.replace("<br>#COMPLETE_TXT#", "");

						info += "  <tr>";
						info += "     <td>";
						
						var sSeq = this.s_seq_nm;
						sSeq = sSeq.replace("임업_", "");
						sSeq = sSeq.replace("화학_", "");

						info += "		" + sSeq;
						info += "     </td>";
						info += "     <td>" + (this.bb_code_nm == null ? "" : this.bb_code_nm) + "</td>";
						info += "     <td>" + this.bb_name + "</td>";
						info += "     <td>" + this.bb_agency_name + "</td>";
						info += "     <td>" + this.bb_visit_time + "</td>";
						info += "     <td>#COMPLETE_TIME#</td>";
						info += "     <td>" + this.bb_txt + "<br>#COMPLETE_TXT#</td>";
						info += "  </tr>";
					} else {
						info = info.replace("#COMPLETE_TIME#", this.bb_visit_time);
						info = info.replace("#COMPLETE_TXT#", this.bb_txt);
					}
                });

				info = info.replace("#COMPLETE_TIME#", "");
				info = info.replace("<br>#COMPLETE_TXT#", "");

                $("#TransTable").find("tbody").html(info);
                var NowPage = $("#Search_Form").find("input[name=Page]").val();
                var page_viewList = Paging(maxCnt, '<%=PageSize%>', '<%=BlockSize%>', NowPage);
                $("#Paging_MainTransForm").html(page_viewList);
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$(document).on("click", "nav a[name=pageGoto]", function (e) {
            var page = $(this).attr("target-page");
            $("#Search_Form").find("input[name=Page]").val(page);
            $("#Search_Form").submit();
        });

        $("#btnExcel").on("click", function (e) {
            var params = $("#Search_Form").serialize();
            params += "&startDate=" + $("#startDate").val();
            params += "&endDate=" + $("#endDate").val();

            location.href = "/data/LOG/Sales.List.Excel.asp?" + params; 
		});

        $("#btnViewExcel").on("click", function (e) {
            var params = $("#Search_Form").serialize();
            params += "&startDate=" + $("#startDate").val();
            params += "&endDate=" + $("#endDate").val();

            location.href = "/data/LOG/Sales.List.View.Excel.asp?" + params; 
		});

	});

//-->
</script>

<!-- #include virtual = "/share/include/PageFooter.asp" -->
